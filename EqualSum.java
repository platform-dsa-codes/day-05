import java.io.*;
import java.util.*;
import java.lang.*;

class GFG {
    public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int test = Integer.parseInt(br.readLine());
		while(test-- > 0) {
			int N = Integer.parseInt(br.readLine());
			int [] A = new int[N];
			String [] str = br.readLine().trim().split(" ");
			for(int i = 0; i < N; i++)
				A[i] = Integer.parseInt(str[i]);
			Solution ob = new Solution();
			System.out.println(ob.equalSum(A, N));
		}
    }
}

class Solution {
    int equalSum(int[] A, int N) {
        if (N == 1) {
            return 1;
        }

        int totalSum = 0;
        for (int i = 0; i < N; i++) {
            totalSum += A[i];
        }

        int leftSum = 0;
        int rightSum = totalSum - A[0];

        for (int i = 1; i < N; i++) {
            if (leftSum == rightSum) {
                return i;
            }

            leftSum += A[i - 1];
            rightSum -= A[i];
        }

        return -1;
    }
}
