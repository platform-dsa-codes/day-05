class Solution {
    public int pivotIndex(int[] dp) {
        int n = dp.length;
        for (int i = 1; i < n; i++) {
            dp[i] += dp[i - 1];
        }
        if (dp[n - 1] - dp[0] == 0)
            return 0;
        for (int i = 1; i < n - 1; i++) {
            if (dp[i - 1] == dp[n - 1] - dp[i])
                return i;
        }
        if (dp[n - 2] == 0)
            return n - 1;
        return -1;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] arr1 = {1, 7, 3, 6, 5, 6};
        System.out.println("Test Case 1: " + solution.pivotIndex(arr1));

        int[] arr2 = {1, 2, 3};
        System.out.println("Test Case 2: " + solution.pivotIndex(arr2));

        int[] arr3 = {2, 1, -1};
        System.out.println("Test Case 3: " + solution.pivotIndex(arr3));
    }
}

